<?php

namespace Drupal\sakmod\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FooterData' block.
 *
 * @Block(
 *  id = "footer_data",
 *  admin_label = @Translation("Footer data"),
 * )
 */
class FooterData extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['footer_data']['#markup'] = 'Copyright © 2019 All right reserved';

    return $build;
  }

}
