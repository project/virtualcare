<?php

namespace Drupal\sakmod\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\Query\QueryFactory;

/**
 * Provides a 'TotalAppointments' block.
 *
 * @Block(
 *  id = "appointment_summary_block",
 *  admin_label = @Translation("Appointment Summary Block"),
 * )
 */
class TotalAppointments extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $account;
  protected $entityQuery;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, QueryFactory $entityQuery) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
    $this->entityQuery = $entityQuery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity.query')
    );
  }

  /**
   * Build method of class TotalAppointments.
   */
  public function build() {
    $userid = $this->account->id();;
    // Getting total appointments for current doctor.
    $totalcount = $this->entityQuery->get('node')->condition('type', 'appointment')
      ->condition('field_healthcare_professional', $userid)
      ->execute();
    $totalcount = count($totalcount);
    // Getting total confirmed appointments for current doctor.
    $totalconfirmed = $this->entityQuery->get('node')->condition('type', 'appointment')
      ->condition('field_healthcare_professional', $userid)
      ->condition('field_status', 'Approved')
      ->execute();
    $totalconfirmed = count($totalconfirmed);
    $build = [];
    $build['#theme'] = 'appointment_summary_block';
    $build['#total_appointments']['#markup'] = $totalcount;
    $build['#confirmed_appointments']['#markup'] = $totalconfirmed;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access summary block content');
  }

  /**
   * Disabling cache of block.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
