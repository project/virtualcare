CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
INTRODUCTION
------------

This Distribution provides a Basic Drupal Healthcare site for interaction between Patient and Healthcare Professional which provides following Features according to User Roles:-
1. Patient Features:
   �Patient can take appointment of any Healthcare Professional and can edit that appointment before doctor approves or rejects it.
   �At the time of taking appointment patient will get on screen message if Healthcare Professional is not available on required date.
   �Patient can add his/her favorite links to homepage for quick access to important pages.
   �There is message center for interaction between patients and Healthcare Professionals where patient can view and create messages.
   �Patients have separate user profile for personal information.

2. Healthcare Professional Features:
   �Healthcare Professional can reject or approve appointments created by patient.
   �Healthcare Professional has separate user profile for professional information.
   �Healthcare Professional can set his/her availability date-time on weekly/daily basis and also for specific dates. 
   �No appointments could be created by pateints if Healthcare Professional is not available on that specific date.
   �Healthcare Professional can set Todo list for listing his/her important tasks.
   �Healthcare Professional can add his/her favorite links to homepage for quick access to important pages.
   �Healthcare Professional can see total appointments and confirmed appointments count on homepage.

3. Editor Features:
   �Editor can add Homepage Banners which is slideshow of images with internal/external links.
   �Editor can add Additional Resources on the homepage which are informational links.
   �Editor can edit the Banner or additional Resources Created by himself/herself.
   �Banner or Additional Resources Created by Editor are saved as draft and it has to be published by site-admin in order to display it on homepage.
4. Site-admin Features:
   �Site-admin can add Homepage Banners which is slideshow of images with internal/external links.
   �Site-admin can add Additional Resources which are informational links on the homepage.
   �Site-admin can edit any Banner or additional Resources.
   �Site-admin can publish the Banner or Additional Resources Created by Editor or by himself/herself in order to display it on homepage.
   �Site-admin can also stop Banner or Additional Resources from displaying on homepage by managing content moderation.

 * For a full description of the distribution, visit the project page:
   https://www.drupal.org/project/virtualcare
 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/virtualcare
REQUIREMENTS
------------

NA

RECOMMENDED MODULES
-------------------
NA

INSTALLATION
------------
 
 * This distribution get installed in the same manner as other distribution does.

CONFIGURATION
-------------
 
 * No special configuration is required



